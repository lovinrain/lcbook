# Table of contents

* [Introduction](README.md) 
* [Algorithm](algorithms/summary.md)
    * [tree](algorithms/tree/tree_summary.md)
        * [binary index tree](algorithms/tree/bit_tree.md)
        * [Segment Tree](algorithms/tree/segment_tree.md)
    * [Math](algorithms/maths/summaries.md)
    * [Topics](algorithms/topic_summaries/summaries.md)
        * [longest/lowest series](algorithms/topic_summaries/L_____series.md)
        * [Range query and LCA](algorithms/topic_summaries/rangequeries_and_lca.md)

* [TEST PAGE](TEST.md)