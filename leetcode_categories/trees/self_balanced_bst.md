# Types
AA tree,Andersson's red back tree, 2-3 tree (Btree variant), scapegoat tree, splay tree, red-black tree, treap

# AVL or RedBlackBst
Reddit: https://www.google.com/search?ei=EGiZXMi_Mor5-wTD0rTQBw&q=avl+tree+red-black+tree+reddit&oq=avl+tree+red-black+tree+reddit&gs_l=psy-ab.3...534465.535952..536385...0.0..0.61.114.2......0....1..gws-wiz.rpj6eVLhWdg
Reddit threads: https://www.reddit.com/r/programming/comments/853xzs/addressing_the_redblack_vs_avl_tree_performance/
AVL good at lookup; RedBlackBst less rotation: https://stackoverflow.com/questions/13852870/red-black-tree-over-avl-tree  https://stackoverflow.com/questions/5288320/why-is-stdmap-implemented-as-a-red-black-tree
Have you been asked during interview: https://www.teamblind.com/article/Have-you-been-asked-red-black-tree-AVL-tree-in-interviews-bFoyDyDR
Why are red black tree so popular: https://cs.stackexchange.com/questions/41969/why-are-red-black-trees-so-popular/54808
Left-Leaning Red-Black Trees Considered Harmful: http://read.seas.harvard.edu/~kohler/notes/llrb.html
# AVL
如果从parent/uncle node开始算起控制，其实会弄得更复杂，所以比较好的方式还是recursive 
<br/> Balanced Factor: H(node.right) - H(node.left); height is defined as the "edges" between root to the furthest leaf.
<br/> AVL force the balanced factor -1, 0, 1

## Unbalacned Cases(Balanced Factor += 2)
### Left Left Case
### Left Right Case

## Operations
### Right rotation
左边太重了，要把ROOT转移到右边

- 左树的右节点转移到右边-> 变新的右树的左节点
```
function rightRotate(A):
    B := A.left #point to new root
    A.left = B.right #左树右节点变成新右树的左节点
    B.right = A # 确定新右树
    return B # Why return B? It is possible that before the rotation node A had a parent whose left/righ pointer referenced it. It is very important taht this link to be updated to reference B. This is usually done on the recursive callback using the return value of rotateRight.
```

### Left rotation
右边太重了，要把root转移到左边
- 右树的左节点转移到左边