#### Facebook原题: move coins
- Observation: 
    - As long as we don't go through duplicate paths, and every node have 1, we are fine. 
    - Therefore can make assumptions based on this
#### 
```java
int moveCoins(TreeNode root) {
        return dfs(root, new HashMap<>());
    }

    int dfs(TreeNode n, Map<TreeNode, Integer> count) {
        if(!count.containsKey(n)) {
            count.put(n, n.val);
        }
        int coinsNum = count.get(n);
        int res = 0;
        for(TreeNode kid : n.children) {
            res += dfs(kid, count); // cost for this kids' downstream
            res += Math.abs(count.get(kid)); // fulfill this kid's need(1 step away)
            coinsNum += count.get(kid); // surplus or deficit
        }
        count.put(n, coinsNum - 1); // my total surplus or deficit; means I have taken care of my descendants. will be fulfilled by my father
        return res;
    }
```