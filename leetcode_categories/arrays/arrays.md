- [总结](#总结)
        - [说明](#说明)
        - [方法](#方法)
        - [注意点](#注意点)
- [#Plaeholder](#plaeholder)
        - [大意](#大意)
        - [思路](#思路)
        - [代码](#代码)
- [#Remove Duplicate from sorted array](#remove-duplicate-from-sorted-array)
        - [大意](#大意)
        - [思路](#思路)
        - [代码](#代码)
- [#Remove Duplicate from Sorted array 2](#remove-duplicate-from-sorted-array-2)
        - [大意](#大意)
        - [思路](#思路)
        - [代码](#代码)
- [#Search in Rotated Sorted Array](#search-in-rotated-sorted-array)
        - [大意](#大意)
        - [思路](#思路)
        - [代码](#代码)
- [#Search in Rotated Sorted Array II](#search-in-rotated-sorted-array-ii)
        - [大意](#大意)
        - [思路](#思路)
        - [代码](#代码)
- [#Plaeholder](#plaeholder)
        - [大意](#大意)
        - [思路](#思路)
        - [代码](#代码)
- [#Plaeholder](#plaeholder)
        - [大意](#大意)
        - [思路](#思路)
        - [代码](#代码)

# 总结
### 说明
无
### 方法
1. 双指针
2. binary search
3. 
### 注意点


# #Plaeholder
### 大意
### 思路
### 代码

# #Remove Duplicate from sorted array
### 大意
Example: A = [1,1,2] return [1,2]
### 思路
因已经排序，用选择排序的思路插入指针所指的该插入的位置即可
### 代码
* 提示: distance(A, unique(A, A+n)) \
* 提示: while(first != last) \
  find_if(first, last, bind1st(not_equal_to<int>(), *first))

# #Remove Duplicate from Sorted array 2
### 大意
最多允许duplicate两次 Example A[]
### 思路
### 代码


# #Search in Rotated Sorted Array
### 大意
`如题`
### 思路
二分: (1) 要看转了之后的形状 (2) 再看转了之后target是否可以根据左右边界和mid来确定，总共4种情况，可以适当合并一下
### 代码
`口答级别`

# #Search in Rotated Sorted Array II
### 大意
Contains duplicate
### 思路
区别在于无法确认区间， 所以要确认的话只有(1) 一直看下去，直到边界 (2) 或者一直看下去，看到得到提示
### 代码

# #Plaeholder
### 大意
### 思路
### 代码

# #Plaeholder
### 大意
### 思路
### 代码