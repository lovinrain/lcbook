# 单链表
### 说明
```C
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x):val(x), next(nullptr) {}
};
```
### 注意点


# #Add Two Numbers
### 大意
**TODO**
### 思路
### 代码
```python
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        dummy = ListNode(0)
        current, carry = dummy, 0

        while l1 or l2:
            val = carry # essentially (l1(or 0) + l2(or 0) + carry) % 10
            if l1:
                val += l1.val
                l1 = l1.next
            if l2:
                val += l2.val
                l2 = l2.next
            carry, val = divmod(val, 10) # divmod!
            current.next = ListNode(val)
            current = current.next

        if carry == 1:
            current.next = ListNode(1)

        return dummy.next
```

# #Reverse linked list II
### 大意
### 思路
![reverse_linked_list_II][reverse_linked_list_II]
需要三个指针, prev, current, looking_ahead
### 代码




[reverse_linked_list_II]: linked_list_1.png "图解"