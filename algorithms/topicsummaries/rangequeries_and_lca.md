# Range queries, Range minimum quries
## 记法： 常规想法是(1)分治 (2) DP
## 常规做法和他们的复杂度
所谓的常规做法其实就是数组
- Naive: O(n^3), O(1), O(n^2) space
- Naive + DP: O(n^2), O(1), o(n^2) space
- 简单分治优化: O(N), O(sqrt(N))
接下去是数据结构改良的优化版
- 用sparse table + meta dp, 如果优化construction, 则是O(N(logN)), O(1), O(Nlog(N)) space
- 用segment tree,  能继续优化construction和space, 但是要放弃O(1)的query: O(N), O(logN), take O(N) space
reduce两次
- RMQ reduce to LCA on Cartesian tree, 再用RMQ解，可以用O(N), O(1) 解答RMQ, 这个恐怕是冷知识了, 放弃了改动

# LCA
- Naive, traverse based: query time 其实是O(h)
    - 方法1, based on array:
        - 做法:直接在树上遍历，存下来到array, 比较array的common fix 
    - 方法2, based on tree: The node which has one key present in its left subtree and the other key present in right subtree is the LCA.
    - 如果是BST，则非常方便，preorder DFS, 从开始到后来，第一个遇到的就是, 可以用recursion就显得更简单了, **这个其实就是利用了binary tree的root应该左右子树各包含一个node的想法，只不过bst里只要比较值的大小就可以了. 看参考资料lowest-common-ancestor-in-a-binary-search-tree
    - 参考资料: 
        - https://www.geeksforgeeks.org/find-distance-between-two-nodes-of-a-binary-tree/
        - https://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/
        - https://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-search-tree/

- Divide and conquer
- Reduce to RMQ: build using Euler tour | query time should be RMQ complexity


# 文章的take-up
RMQ:
1. 学会dp
2. 学会分治
3. 学会meta search version的dp， refer to Topic [BinarySearch](../singlecategories/BinarySearch.md)

LCA:
1. 学会meta version的dp
2. 学会分治
3. 学会把别人拉到同一个level，一起找爸爸

Reduction:
1. From RMQ to LCA to RMQ

# 文章的算法
- Meta search
    - https://www.quora.com/What-is-meta-binary-search
    其实就是用bit来表示index, 然后每次就确定一个bit, 但是要算两次，一共两份文档详见[BinarySearch](../singlecategories/BinarySearch.md)
- four russians
    - https://en.wikipedia.org/wiki/Method_of_Four_Russians
- Tarjan's algorithm: euler tour to RMQ
- Monotonical stack to build the cartesian tree
