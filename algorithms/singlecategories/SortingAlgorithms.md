* Radix, bucket, count sort

资料:
https://www.byvoid.com/zhs/blog/sort-radix
https://www.jianshu.com/p/ff1797625d66

# Count Sort
记法: 原任务:
- 原数字在A中；
- 要放到B中
- 借助C
算法: 用C生成了累加数组，那么就用它给A中的数倒着找下家， 
1. 取A中的数，倒着取
1. C 相当于就是index数组, index对应存的是A中所有不大于这个数的个数
1. 因此就可以安心地把这个位置当作放入该放的地方
1. 可以增加一个Order数组来保存真正的A中对应的位置(order表示B中的那个数在A中的位置)
```
    for (i=1;i<=N;i++) //把A中的每个元素分配
        C[A[i]]++;
    for (i=2;i<=K;i++) //统计不大于i的元素的个数
        C[i]+=C[i-1];
    for (i=N;i>=1;i--)
    {
        B[C[A[i]]]=A[i]; //按照统计的位置，将值输出到B中，将顺序输出到Order中
        Order[C[A[i]]]=i;
        C[A[i]]--;
    }
```
# Bucket
- 如果C除去累加的那一下, 直接线性扫描，就是bucket sort
- 如果要稳定，需要建linkedlist,倒着插入，就可以顺着取出
# 基数排序
略 o(d(n+k))

TODO: 待整理google doc

