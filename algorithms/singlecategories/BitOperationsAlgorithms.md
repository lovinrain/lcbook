# Bit operation
- 负数的表达为溢出的值的补码，也就是反码+1, 原因是如果只按最高位，则0有两个表示，正0和负0， 事实上的负0应该为最大的负数
    - 比如 001的附属是1000 - 001 = 111
- least significant bit
    - Integer.lowestOneBit()
    - i & -i, 因为负数是反码+1， 那个+1没有被抵消| 或者想想i + -i 要进位，则一定有两个1，而这两个1之前都分别是0，1
- 数组表示数结构时，如果想得到末尾的两个组合:     for (i += N; i > 1; i >>= 1) { tree[i>>1] = function(tree[i], tree[i^1]); }
    [E.g. Segment tree](https://github.com/williamfiset/data-structures/blob/master/com/williamfiset/datastructures/segmenttree/CompactSegmentTree.java)
- 异或
    - 和0做异或: 原数
    - 和1做异或: 各位取反 | 如果是^1,  则是末位取反，可以用与数组构成的树里找index, 根据原数，可能+1也可能-1, 适合做树形数组的index
    - 任何数异或自己＝把自己置0
    - 通过按位异或运算，可以实现两个值的交换，而不必使用临时变量。例如交换两个整数a，b的值，可通过下列语句实现：
    - 异或运算符的特点是：数a两次异或同一个数b（a=a^b^b）仍然为原值a
    - https://blog.csdn.net/kybd2006/article/details/3727218
