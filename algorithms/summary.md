# Algorithm is combined with Data structures


# Useful Sources(current on the list of cracking)
## Competitive
- https://cp-algorithms.com
## Advanced
http://people.csail.mit.edu/moitra/854.html:
Sanjeev Arora's Advanced Algorithm Design, Princeton
Michel Goemans's Advanced Algorithms, MIT
David Karger's Advanced Algorithms, MIT
David Karger's Randomized Algorithms, MIT
Ryan O'Donnell's Advanced Algorithms, CMU
Tim Roughgarden and Greg Valiant's The Modern Algorithmic Toolbox, Stanford
Thomas Vidick's Advanced Algorithms, Caltech