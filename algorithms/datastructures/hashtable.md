#### 定义
- Perfect hashing
- Universal hashing
- Consistent hashing
##### Comparison
In Universal Hashing you define a family of hash functions that have some degree of independence among them. In a very simplified way universal hashing is useful when you need not one but many hash functions as you can pick different functions from the family and use them.

In contrast perfect hashing implies building a hash function without collisions. So for each key in your dataset a unique h(key) value is produced.



hash fucntion design
hash的集中方法(soulemachine)
hash extension
geohash
cucoohash
一致性哈希
https://soulmachine.gitbooks.io/system-design/content/cn/hashmap.html
随机数
twitter snowflake (可以看看吴老的slides)

MurmuHash
Google CityHash
GoogleFarmHash
CLHash
CRC-32
SimHash
GeoHash
Cuckoo hashing

[浙江大学数据结构冲突处理方法11_3_4]:https://zhuanlan.zhihu.com/p/34520264
[哈希算法和解决哈希冲突的四种方法]: https://www.jianshu.com/p/b9e6015703c2?utm_campaign=maleskine&utm_content=note&utm_medium=seo_notes&utm_source=recommendation
[不知名slides提纲挈领from_uuse]:https://www.it.uu.se/edu/course/homepage/avalgo/vt11/PerfectHash.pdf