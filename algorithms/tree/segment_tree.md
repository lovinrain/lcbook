# Segment Tree基本版 + lazy propagation
## 常见用途
- range query: +, *, min, log(n)求,  log(n) update[bottom up], 


# Implementation总结
## Naive implementation
- 使用2N space的tree construction, 和heap差不多; 据说worst case是4N **WHY????**
- Code 略

## 更高更快的技巧
## lazy propagation
- 用途：当update很多的时候, 比如range update, 原树O(nlog(n))
- 大意: 存一个备用数据结构记录还没有udpate的， 针对range update, 在total overlap的时候，可以尽可能少地更新node | 算法还是原来的方法来，不同的是，在搜寻或者update的操作中，途径一些点的时候，看到备用数据结构没有propagate的需要向下push, 再向上更新。
- Implementation(see code): 
      - 无论是query 还是update, 路过点的时候，先根据lazy tree有没有mark而update以前遗留的，把该点处理
      - update的时候, 
            - 路过点的时候，先根据lazy tree有没有mark而update以前遗留的，把该点处理
            - 对于该轮的update 其实就是
                  - total overlap的时候要多看一下并且update一下lazy tree
                  - partial overlap的时候先处理完下面的(这样至少两个immediate child是最新的)，更新一下主树
      - Query:
            - 一定要先"路过点的时候，先根据lazy tree有没有mark而update以前遗留的，把该点处理"

# 记忆方式
关键词: partial oiverlap, total overlap, no overlap, node自己是主语，看自己是不是overlap在了search 范围内，不是的话就往下看
## 基本版Skeleton code
其实就是给一个range，然后敲打node，问这个range你能贡献多少；如果全在里面(相当于已经可以回答)，那交出所有，如果没有交集，0， 不然从儿子里找
```python
  int getSum(node, l, r) 
  {
    if the range of the node is within l and r
          return value in the node
    else if the range of the node is completely outside l and r
          return 0
    else
      return getSum(node's left child, l, r) + 
            getSum(node's right child, l, r)
  }
```
![segment_tree1](./pics/segment_tree_1.png)
![segment_tree2](./pics/segment_tree_2.png)
![segment_tree3](./pics/segment_tree_3.png)

# 资料
## Video
[Tushay segment tree][[Tushay segment tree]]
[Lazy propagation tutorial][Tushay lazy propagation]

# TODOs
## Segment Tree加强版 以及他们的lazy propagation
## 2D的segment tree


[Tushay segment tree]:https://www.youtube.com/watch?v=ZBHKZF5w4YU&t=213s
[Tushay lazy propagation]: https://www.youtube.com/watch?v=xuoQdt5pHj0&t=7s
[bit vs segment]:https://www.quora.com/What-are-the-advantage-of-binary-indexed-tree-BIT-or-fenwick-tree-over-segment-tree
[Quora: faster non-recursive segment tree explanation]:https://www.quora.com/profile/Md-Shahbaz-Shafi-1
[Codeforce: Everything about segment tree]:http://codeforces.com/blog/entry/15890
[Codeforce: segment tree]:http://codeforces.com/blog/entry/3327
[Codeforce: Efficient and easy segment trees]: http://codeforces.com/blog/entry/18051
[Codeforce: Non-recursive Implementation of Range Queries and Modifications over Array]:https://codeforces.com/blog/entry/1256
[Top Coder RMQ and LCA]: https://www.topcoder.com/community/competitive-programming/tutorials/range-minimum-query-and-lowest-common-ancestor/
[WilliamFiset interface]: https://github.com/williamfiset/data-structures/blob/master/com/williamfiset/datastructures/segmenttree/CompactSegmentTree.java