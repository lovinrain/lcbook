联动 [Segment Tree](segment_tree.md)

# 常见用途, 所的解决问题
- 假设一个indxed的数组，进行如下range query操作, 则可以用segment tree解决： min, sum(i.e. +)等associate的操作
- 如果这个operator是invertible function, 就是知道结果和一个input可以推出另外一个input的， 比如sum(+, 可以用和和一个输入求出另外一个输入)，则可用bit， 所以bit能解决的问题是segment的子集
- 复杂度对于下列操作符合要求
    - range query, point update: BIT: O(log(n))求prefix sum, O(log(n))插入, 可以跟naive的算法比较一下
    - range update, point query: O(log(n)), O(log(n)) : **BIT can easily adapted to this, see code, 原理就是基于sum的快速计算**
- Bit比segment tree好的地方：[必看文章][bit vs segment]
    - Memory efficient
    - Easier to code
    - 要注意的是，从速度上比对，并没有太大优势


# 前提知识和观察
- 任何数都有二进制形式，所以一定能转换为二进制和的形式(这就是二进制的意义)
- 但是二进制很特殊，所以和也很特殊，是有一些 1开头，一个或者多个0结尾的数的和, 而且前面没有系数
- 所以换句话说，任何十进制数，都可以转化为一些2^i的和, 而且组合唯一
- 所以任何数以二进制形式看，都是1xxxx的数的和组合

# BIT的解决方案
- range query, point update: 建立树，每个node有值和序号，序号于对于原数组的index，查询和插入就变成了找出index对应序号的一些node和他们的值，对树上的node进行操作, 原数组可以用于直接的查询
- range update, point quries: **略，see code**
- 不清楚的可以看一下视频动画，或者 [代码用法][WilliamFiset interface]

# 数据结构总结
## 结构
- 每一个node都有它"负责"的一小部分， 这个数量就由node的序号的最右边的1和后面的0的个数共同决定; node序号从1开始数
- 每个node的前驱，就是消1， 一只消下去能得到prefixsum, 每个node的后驱，其实就是后边一串0中选一个置一
- 每个node的要更新的最近的节点就是自己+自己负责的部分
## 操作
- 关于update: 只需要update所关系到的那些部分，算法就是循环i+lsb(i), 这就是最容易忘记的部分了, 用大白话讲就是找负责自己的那个father, 因为已经说过，自己负责的是最后右边的1和后面的0，负责自己的，其实就是(自己+自己负责的)，即自己负责的*2, 这个体会不了就背吧，不过记住了图应该能体会
- prefixsum: 逐个消1，再求那些index的和,和就是所有1的构成所在的数的和， 比如1101, 就是1000, 100, 1的和 (log n)

# Implementation重点总结
- implement LSB(): i & -i  (或者)
- prefixsum **O(logn)**: 逐个消1，再求那些index的和: while(i !=0) { sum += tree[i]; i -= lsb(i)}
- update **O(logn)**: while(i < tree.length) {tree[i] +=k; i+= lsb(i)}
- construction **O(n)**: Array implementation; bottom up: 遍历数组，根据需要建的树的哪些node负责自己，来去更新(update)他们, 但是因为是遍历和第一次建立，之后的father一定会遍历到，所以其实只需要更新自己的father 算法 i+lsb(i) **要注意的是** construction only takes O(n)，用了技巧以后，而不是O(nlogn)， 这和heap construction O(n)一起背一下


# 材料
## video
- [图的由来 WilliamFiset](https://www.youtube.com/playlist?list=PLDV1Zeh2NRsCvoyP-bztk6uXAYoyZg_U9)
## Code
- [WilliamFiset interface][WilliamFiset interface]
- [Range query(basic)][range query]
- [Range update][range update]
## 文章
- [Binary index tree vs Segment tree, 必看][bit vs segment]
- [TopCoder article](https://www.topcoder.com/community/competitive-programming/tutorials/binary-indexed-trees/)

# 记忆方式
关键词: lsb, prefixsum, responsible part

![bit_tree_memory_1][bit_tree_memory_1]
![bit_tree_memory_2][bit_tree_memory_2]
![bit_tree_memory_3][bit_tree_memory_3]

[bit_tree_memory_1]: ./pics/bit_tree_1.png "图解"
[bit_tree_memory_2]: ./pics/bit_tree_2.png "图解"
[bit_tree_memory_3]: ./pics/bit_tree_3.png "图解"

# TODOs
## Lazy propagation
**TODO**

## 2-D version
**TODO**

[WilliamFiset interface]: https://github.com/williamfiset/data-structures/tree/master/com/williamfiset/datastructures/fenwicktree
[range query]:(https://github.com/williamfiset/data-structures/blob/master/com/williamfiset/datastructures/fenwicktree/FenwickTreeRangeQueryPointUpdate.java)
[range update]:https://github.com/williamfiset/data-structures/blob/master/com/williamfiset/datastructures/fenwicktree/FenwickTreeRangeUpdatePointQuery.java
[bit vs segment]:https://www.quora.com/What-are-the-advantage-of-binary-indexed-tree-BIT-or-fenwick-tree-over-segment-tree