* KMP
* Boyer-Moore
BM总结:就是移动是用herustic的位置
      [[http://www.ruanyifeng.com/blog/2013/05/boyer-moore_string_search_algorithm.html]]
      [[http://www.ruanyifeng.com/blog/2013/05/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm.html]]

BM vs KMP vs AC
[https://softwareengineering.stackexchange.com/questions/183725/which-string-search-algorithm-is-actually-the-fastest]

Naive vs KMP vs BM vs Z-algorithm
[https://www.quora.com/What-is-the-best-algorithm-for-pattern-searching]

KMP对dna搜索比较好， BM对全文搜索比较好，原因是因为bm考虑了全局的pattern processing
[https://stackoverflow.com/questions/12656160/what-are-the-main-differences-between-the-knuth-morris-pratt-and-boyer-moore-sea]
[http://www.cs.utexas.edu/~moore/best-ideas/]
[http://www.cs.utexas.edu/~moore/best-ideas/string-searching/kpm-example.html#step04]
[http://www.cs.utexas.edu/~moore/best-ideas/string-searching/fstrpos-example.html]
* Rabin-karp (rolling Hash)
* Z-algorithm
* AC自动机

# 后缀数组
- 典型应用: longest common substring