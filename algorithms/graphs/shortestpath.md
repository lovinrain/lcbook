# TODO: 待整理: GOOGLE DOC SPFA | bellmanford



## Dijkstra
- Dijkstra 不能处理negative edge的例子原因：贪心不会去更新已经确定的值，没有relaxation
![nagative](./pics/shortestpath_1.jpg)
资料
- https://www.zhihu.com/question/21620069
- https://www.zhihu.com/question/57206374
- https://www.quora.com/What-is-the-simplest-intuitive-proof-of-Dijkstra’s-shortest-path-algorithm : 太奇葩了fibonacci的加入和decrease key value都是O(1), remove是log n
- https://www.quora.com/What-is-the-complexity-of-Dijkstras-algorithm
Dijkstra复杂度:
- V， E给定，但是 E capped by V (O(V^2)) 所以 V < E < V^2
- V个动态insertion用于初始化最开始的queue, E个decrease key, v个deletion from queue
- binary heap implementation: insertion: V * O(log(V)) | decrease key | E* O(log(V)) | deletion: V * O(log(V)), 最大项是 E * O(log(V)
- fibonacci的话 V + E + V O(log(V)), 最大项是 V O(log(V)

## BellmanFord
https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm
- Wiki applications:
- 其实如果有一轮大家都没有更新，就可以提前结束了 |Since the longest possible path without a cycle can be {\displaystyle |V|-1} |V|-1 edges, 
- Negative cycle的检测就是如果V-1次以后再有distance的减小，则说明有negative edge
- 资料
[How is bellmanford dynamic programming][B1] 指出有两种理解bellman ford的方法，一种是根据和djkstra比较，还有一种是理解为from src to dst的recurrence 


## Floyds and warshall
- Wiki application
- Why does it work
    - 更新k次的Dp， 太绝了
- Why is this dynamic programming
- 材料
    - [MIT2005 6.045][F1V]
    - [MIT2015 6.046][F2V]
    - [MIT2011 6.006]


[B1]: https://www.quora.com/How-is-the-Bellman-Ford-algorithm-a-case-of-dynamic-programming

[F1V]: https://www.youtube.com/watch?v=adkZE-yHSsw
[F2V]: https://www.youtube.com/watch?v=Aa2sqUhIn-E

## SPFA
    - 

## How to find negative edges