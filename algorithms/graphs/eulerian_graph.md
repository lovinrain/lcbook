## Definition
### What is euler path, euler cycle, euler circuit:
- An Euler Path is a path through an undirected graph that visits every edge exactly once.
- An Euler Circuit is an Euler path that begins and ends at the same node.
- Euler cycle = Euler circuit
- [Definition introduction in slides][Columnbia CS3134 Slides]

## 算法
Hierholzer

### 判定
- 出度/入度关系
- path的唯一性?
- Cycle的唯一性?



### 例题：
密码箱问题： http://massivealgorithms.blogspot.com/2016/11/poj-1780-code-google-interview-safe-box.html

Reft找的东西

Reference:
[Columnbia CS3134 Slides]: http://www.cs.columbia.edu/~bauer/cs3134-f15/slides/w3134-1-lecture19.pdf