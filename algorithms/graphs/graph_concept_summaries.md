# 图
## 基本概念

## 常见名词
- Euler circuit
- Euler path
- Hadmilton path: “哈密尔顿回路问题”是访问除原出发结点以外的每个结点一次且仅一次
- 欧拉路径: 欧拉回路问题”是访问每条边一次且仅一次；所谓旅行推销员问题是： 推销员从驻地出发经过所要去的城市至少一次返回原地，应如何安排使其总的旅行距离最短
- 欧拉回路



## 常见问题
- travelingSalesman: 从旅行推销员问题的概念来看它的本质是哈密尔顿圈的应用与延伸若把城市作为一个顶点，哈密尔顿圈只要求过每一个顶点一次且仅一次； 而推销员回路是至少一次必要时允许重复通过。（补充类使问题还有“中国邮路问题”，详细内容见参考资料）。
其实正式的定义还是看wiki比较好:
https://en.wikipedia.org/wiki/Travelling_salesman_problem
里面写着exactly once的要求在之后被remove
本质是一个dp： 
https://blog.csdn.net/bh_xiaoxinba/article/details/52490790
https://www.cnblogs.com/youmuchen/p/6879579.html(这个基本就是解释)
其他的解法:
https://zhuanlan.zhihu.com/p/38996272
https://blog.csdn.net/elite_2007/article/details/2253745
**需要搜更多?**
- post:

- vehicle routing
- 欧拉回路的判定

## CheatSheet
- 

## 基础
### DFS
    复杂度: O(|V| + 2|E|), 每一个edge要检查两次, 作为顶点的两端会各检查一次
#### Edges definition
pre/post ordering for (u, v) in edge
Tree/forward Back Cross
uvvu vuuv vvuu
(Algorithms.pdf page 95)
### BFS
    复杂度: O(|V| + 2|E|), same as in DFS

### 联通分量，强联通分量
- 联通： 如果i到j有路径，且j到i有路径，则联通
- CC: 无向图, 点之间两两联通, 其实只要有一条路径就意味着两两联通
- SCC:
    1. 定义： 有向图，点之间两两联通
    2. 算法： Tarjan， Kosaraju
        * Tarjan 的算法快30%， 因为不用建反向图
            1. Linear-time DFS algorithm(Tarjan)(1972)
            2. easy two-pass linear time algorithm (Kosaraju-Sharir)
            3. 1990s
        * 参考说明: [Byvoid的博文][byvoid_scc] 
        * Tarjan 主要想法就是SCC一定有一个先遍历的节点，把它当根, 最后由它来输出相互之间联系的所有值， 好处就是不用建立反向图 具体如下:
            * 如果不考虑回溯，直接已经在单个scc, 或者scc的其中之一，则其实一定能回去
            * DFN为DFS Number; low为stack中能追溯到的最小的编号
            * DFN[u] == Low[u] 表示现在的这个是一个scc的根(毕竟总有一个“先访问”的节点)，可以一只退栈到当前栈出来的是现在的节点为止(这个比较难想) | 另外不管哪个值出发，其实这个算法总能绕到找出这样一个Low[u] = DFN[u]的 | 多run几个example应该能找出感觉
        * Low(u)=Min\
            {
                DFN(u), 这是scc 只有自己的情况
                Low(v),(u,v)为树枝边，u为v的父节点, 这是说子树能连到自己的祖先的情况，这个情况需要上报
                DFN(v),(u,v)为指向栈中节点的后向边(非横叉边)， 说明直接有条边连到已经访问的上线
            } || 这个表示的是说low(u)只有这几种情况
        * 感觉只有这个算法知道哪里去查找就可以了
### 拓扑排序
- DAG:
    - Reverse order of post-order traversal | Reverse order can be implmemented using a stack
- 完整的推理
    - Property For any nodes u and v, the two intervals [pre(u), post(u)] and [pre(v), post(v)] are
either disjoint or one is contained within the other.
    - Can tell if a edge is Tree/forward, or back, or Cross purely from post order number (page 95)
    - Property A directed graph has a cycle if and only if its depth-first search reveals a back edge.
    - Property In a dag, every edge leads to a vertex with a lower post number. (可以找出上面列出来的方法)
    - Property Every dag has at least one source and at least one sink. (可以有另外一个方法: Find a source, output it, and delete it from the graph.Repeat until the graph is empty.) 归纳一下就是从source开始peel
- 有关SCC的meta-graph排序: 找强联通分量，也叫Kosaraju算法
    - Every directed graph is a dag of its strongly connected components.
    - The node that receives the highest post number in a depth-first search must lie
in a source strongly connected component.
    - If C and C' are strongly connected components, and there is an edge from a node in C to a node in C', then the highest post number in C is bigger than the highest post number in C'
    - 简单地说就是在反向图中跑拓扑排序，然后再DFS找CC, 从sink开始peel


## 网络流
###
- Cut, 割
- Flow, 流
- TODO: Sedgewick



## 二分图
### 基本概念
- matching
- perfect matching
- stable matching
### 无权二分图

## 有权二分图


图的割点、桥与双连通分支




# 实际例题
## 路径
- 一堆密码箱，每个密码都是四位0-9之间，算一个暴力破解序列，包含所有可能的四位序列，让这个序列尽量短 https://www.1point3acres.com/bbs/thread-165566-1-1.html
    - de bruijn sequence


[byvoid_scc]:(https://www.byvoid.com/zhs/blog/scc-tarjan)