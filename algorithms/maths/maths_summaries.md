#### 筛法算和
以20举例
1. 20以内，一个数的最小因子不会大于sqrt(20), 
2. 1....20之间所有以2为最小因子的数的和（包括2）就为10以内数的和，减去1乘以2，同理，所有以3为最小因子的和，就是20/6以内和，减去1， 再减去2的倍数（因为2的倍数的话，不符合“以3为最小因子”），乘以3，
3. 综上，要算n以内所有的数和，根据定义减去1之后，就检查2-n以内的数，被2-sqrt(n)内的素数轮轮番检查每个数（素数的判定一会儿再说），如果这个数的最小因子是 2-sqrt(n)中的一个，则在该轮被删去, 这里有个重要观察，在2-sqrt(n)遍历的过程中，比如遍历到了i， 则i^2内的质数是被筛出来了的，所以算法的目的就是遍历到sqrt(n)结束，而这个过程中希望尽可能地重复利用前面的结论
4. 因为和可以用公式算，而且可以重复利用之前的结论，所以就有了快速算法, 需要倒着来, 主要问题是素数的判定, 很简单，因为2-sqrt(n)是递增的，每一轮，假设是i， 都可以保证i^2以内的质数被筛出, 这个算法的精髓是如何利用之前的计算结果，又不重复计算比如2*3知该在i=2的时候删，而i=3的时候不要删，而且2*2*3则是利用之前结果直接删除
```python
from time import clock
start=clock()
def P10(n):
    r = int(n**0.5)
    assert r*r <= n and (r+1)**2 > n
    V = [n//i for i in range(1,r+1)]
    V += list(range(V[-1]-1,0,-1))
    S = {i:i*(i+1)//2-1 for i in V}
    for p in range(2,r+1):
        if S[p] > S[p-1]:  # p is prime
            sp = S[p-1]  # sum of primes smaller than p
            p2 = p*p
            for v in V:
                if v < p2: break
                S[v] -= p*(S[v//p] - sp)
    return S[n]
print "sum of primes under 10^9:",P10(10**9)
finish=clock()
print finish-start,"seconds"
```
- sieve of Eratosthenes




#### Reservoir Sampling
- 对象为无法在内存中放下的数据, 如不间断数据流, 或者巨大的文件, 数组等.
- 样本集的大小为k, 并且要求每个样本的取样概率相等.
- 取样概率可以通过添加权重(weight)来改变取样概率.
- 一般(无weight)水塘抽样的每个样本的取样概率为: k/(n+1)
- 水塘抽样的算法实现非常简单, 而且证明简练. 算法如下:
  - 详细证明: [[http://www.cnblogs.com/youxin/p/3348225.html]]
  - 其实是归纳法，上面这个网页没有说得很清楚 [[http://blog.csdn.net/my_jobs/article/details/48372399]]
  - Wiki: [[https://zh.wikipedia.org/wiki/%E6%B0%B4%E5%A1%98%E6%8A%BD%E6%A8%A3]]

- Sterling approximation
  - o(nlogn) | o(lg(n!)) 因为 2^h = n!
- Catalarn number approximation
  - O(4^n)