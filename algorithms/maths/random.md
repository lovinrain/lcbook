## Reservoir sampling
最好的办法是直接记忆{k - N} 版本:
若记不住，可以用k =1 版本来回忆
**问题**: 对于在线的数据流，面对新的数据，决定当次每个数被选中的概率相等 
- k=1版本: 每次都以 1/i， 作为“被候选” ， 实际操作可以用[0, i-1] 中选取随机数到0 ||| 记法:  对每个i， 当时这个数的概率就需要是1/i了(留下的数的概率也是这个值)
- k任意版本:  “被候选有k个”， 每次都以k/i作为“被候选", 但主要问题是如何求得这个k/i的概率, 其实就是i的范围内，random数限制在k之间, 还有一个问题就是候选了以后，怎么办，答案是取代原水塘里的其中之一，可以直接重复利用那个k||| 记法: 对于每个i， 当时的数的概率就得是k/i
**为什么叫水塘**: 维持一个k大小的水塘，然后每次根据在线进来的数据进行交换
https://zh.wikipedia.org/zh/%E6%B0%B4%E5%A1%98%E6%8A%BD%E6%A8%A3

以下的链接看几个就理解了:
https://zhuanlan.zhihu.com/p/29178293
https://blog.csdn.net/My_Jobs/article/details/48372399
https://www.cnblogs.com/HappyAngel/archive/2011/02/07/1949762.html
https://www.cnblogs.com/youxin/p/3348225.html
https://www.iteblog.com/archives/1525.html
http://www.chenguanghe.com/reservoir-sampling-%E6%B0%B4%E5%A1%98%E6%8A%BD%E6%A0%B7/
https://handspeaker.iteye.com/blog/1167092
